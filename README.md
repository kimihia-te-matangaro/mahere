# mahere
A basic map-based web app written in pure Javascript, HTML and CSS that uses [OpenLayers](https://openlayers.org/doc/quickstart.html) to display data about [Titles](https://data.linz.govt.nz/table/52067-landonline-title/), [Title Hierarchy](https://data.linz.govt.nz/table/52011-landonline-title-hierarchy/), [Parcel Geometry](https://data.linz.govt.nz/layer/51571-nz-parcels/) and [Parcel Appellation](https://data.linz.govt.nz/table/51590-landonline-appellation/) pulled from the [LINZ Data Service (LDS) API](https://data.linz.govt.nz/).

## LINZ API Key
**The `LINZ_API_KEY` is necessary to run this app**. mahere will prompt the user to enter a key that is used to talk to the LDS. Once mahere is provided with a valid api key, the key is cached in the browser and the user is not prompted again. Equip yourself with an API key from [here](https://www.linz.govt.nz/data/linz-data-service/guides-and-documentation/creating-an-api-key). 

## mapspast
Basemaps consist entirely of work done by Matt Briggs at [mapspast](http://www.mapspast.org.nz/) which provides free and open access to an ArcGIS server where maps are hosted in S3 buckets.

## build
Modules are built with [webpack](https://webpack.js.org/) and are bundled into `dist/bundle.js`.

## deploy 
An Azure `DEPLOYMENT_TOKEN` is stored under `Settings -> CI/CD -> Variables` which allows an Azure instance to deploy the app built by the GitLab CI/CD runners. Any new implementations of mahere will require a token to be deployed. Past builds are available under `CI/CD -> Pipelines`. 

## dev work
To get started: `npm i` `npm run build` `npm run start`


