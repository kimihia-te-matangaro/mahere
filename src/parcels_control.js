import { Control } from "ol/control.js";
import { GeoJSON, WFS } from "ol/format";
import {
  and as andFilter,
  equalTo as equalToFilter,
  like as likeFilter,
  or as orFilter,
} from "ol/format/filter";
import { Style } from "ol/style";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";

export class ParcelsControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    let options = opt_options;

    super({
      target: options.target,
    });

    this.nztm = options.nztm;
    this.vectorLayer = options.vectorLayer;
    this.vectorSource = options.vectorSource;
    this.geo = new GeoJSON({ dataProjection: this.nztm });
  }

  highlightStyle = new Style({
    fill: new Fill({
      color: "rgba(255,255,255,0.4)",
    }),
    stroke: new Stroke({
      color: "#FF69B4",
      width: 3,
    }),
  });

  fetchParcelId(refs, LINZ_API_KEY) {
    let myFilter;
    if (refs.length > 1) {
      myFilter = orFilter(...refs.map((e) => equalToFilter("ttl_title_no", e)));
    } else {
      //assume refs is not empty
      myFilter = equalToFilter("ttl_title_no", refs[0]);
    }
    let result = [];
    //parcel association
    const featureRequest = new WFS().writeGetFeature({
      featureTypes: ["table-52008"],
      request: "GetFeature",
      service: "WFS",
      outputFormat: "application/json",
      version: "2.0.0",
      filter: myFilter,
    });
    return fetch(
      "https://data.linz.govt.nz/services;key=" + LINZ_API_KEY + "/wfs?",
      {
        method: "POST",
        body: new XMLSerializer().serializeToString(featureRequest),
      }
    )
      .then((response) => response.json())
      .then((parcelAssociation) => {
        if (parcelAssociation.features.length > 0) {
          //this.fetchParcelData(result, title);
          return parcelAssociation;
        } else return false;
      });
  }

  fetchParcelData(parcel_ids, title, LINZ_API_KEY) {
    var featureRequest;
    if (parcel_ids.length > 0) {
      var ids = parcel_ids.map((v) => v.toString());
      if (parcel_ids.length == 1) {
        featureRequest = new WFS().writeGetFeature({
          featureTypes: ["layer-51976"],
          request: "GetFeature",
          service: "WFS",
          outputFormat: "application/json",
          version: "2.0.0",
          srsName: "EPSG:2193",
          filter: equalToFilter("id", ids[0]),
        });
      } else if (parcel_ids.length > 1) {
        featureRequest = new WFS().writeGetFeature({
          featureTypes: ["layer-51976"],
          request: "GetFeature",
          service: "WFS",
          outputFormat: "application/json",
          version: "2.0.0",
          srsName: "EPSG:2193",
          filter: orFilter(...ids.map((i) => equalToFilter("id", i))),
        });
      }
      fetch(
        "https://data.linz.govt.nz/services;key=" + LINZ_API_KEY + "/wfs?",
        {
          method: "POST",
          body: new XMLSerializer().serializeToString(featureRequest),
        }
      )
        .then((response) => response.json())
        .then((parcelData) => {
          var geo = this.geo.readFeatures(parcelData);
          geo.forEach((v) => {
            var names = [
              "par_id",
              "maori_name",
              "appellation_value",
              "parcel_type",
              "parcel_value",
              "second_prcl_value",
            ];
            v.setStyle(this.highlightStyle);
            v.set("title_no", title);
            this.fetchAppellation(v.get("id"), LINZ_API_KEY).then((appellation) => {
              //each appellation in the response for a given parcel id
              appellation.forEach((a) => {
                names.forEach((n) => {
                  /** if response is not none
                   * if feature already has a given property and its not null
                   * if new property is different from current add to array if not same value
                   **/

                  if (a.properties[n] != null) {
                    if (v.get(n) != null) {
                      const propertiesArray = [];
                      if (a.properties[n] !== v.get(n)) {
                        propertiesArray.push(a.properties[n]);
                        propertiesArray.push(v.get(n));
                        v.set(n, propertiesArray);
                      }
                    } else v.set(n, a.properties[n]);
                  }
                });
              });
            });
          });
          //this.vectorSource.clear();
          this.vectorSource.addFeatures(geo);
        });
    } else alert("no parcel data available for parcel: ", parcel_ids);
  }

  fetchAppellation(parcel_id, LINZ_API_KEY) {
    const featureRequest = new WFS().writeGetFeature({
      featureTypes: ["table-51590"],
      request: "GetFeature",
      service: "WFS",
      outputFormat: "application/json",
      version: "2.0.0",
      filter: equalToFilter("par_id", parcel_id.toString()),
    });
    return fetch(
      "https://data.linz.govt.nz/services;key=" + LINZ_API_KEY + "/wfs?",
      {
        method: "POST",
        body: new XMLSerializer().serializeToString(featureRequest),
      }
    )
      .then((response) => response.json())
      .then((parcel) => {
        var result = [];
        if (parcel.features.length > 0) {
          parcel.features.forEach((v) => {
            result.push(v);
          });
        } else console.log("EMPTY APPELATION FOR PARCEL: ", parcel_id);
        return result;
      });
  }

  removeParcel(parcel_ids) {
    var allFeatures = this.vectorSource.getFeatures();
    allFeatures.forEach((f) => {
      if (parcel_ids.includes(f.get("id"))) {
        this.vectorSource.removeFeature(f);
      }
    });
  }
}
