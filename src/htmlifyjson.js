export default function htmlifyJSON(data) {
  if (data === undefined) return undefined;
  else if (data === null) return "null";
  else if (data.constructor === String) {
    return data;
  } else if (data.constructor === Number) return String(data);
  else if (data.constructor === Boolean) return data ? "true" : "false";
  else if (data.constructor === Array)
    return (
      "<ul><li>" +
      data
        .reduce((acc, v) => {
          if (v === undefined) return [...acc, "null"];
          else return [...acc, htmlifyJSON(v)];
        }, [])
        .join("</li><li>") +
      "</li></ul>"
    );
  else if (data.constructor === Object)
    return (
      "<dl>" +
      Object.keys(data)
        .reduce((acc, k) => {
          if (data[k] === undefined) return acc;
          if (data[k] === null) return acc;
          else
            return [
              ...acc,
              "<dt>" +
                htmlifyJSON(k + ":") +
                "</dt><dd>" +
                htmlifyJSON(data[k]) +
                "</dd>",
            ];
        }, [])
        .join("") +
      "</dl>"
    );
  else return "{}";
}
