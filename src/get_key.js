export default function get_key(message){
    var LINZ_API_KEY;
    var proceed = true;
    while(proceed){
        let key_input = prompt(message, "")
        if (key_input != null){
            LINZ_API_KEY = key_input;
            proceed = false;
        }
    }
    return LINZ_API_KEY;
}