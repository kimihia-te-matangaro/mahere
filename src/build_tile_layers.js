import LayerGroup from "ol/layer/Group";
import TileLayer from "ol/layer/Tile";
import XYZ from "ol/source/XYZ";

/*
gets passed projection that map is using 
returns a group of layers called baseMaps
*/
export default function build_tile_layers(nztm) {
  // var mapspast = 'http://au.mapspast.org';

  var nzms13_1899,
    nzms13_1909,
    nzms13_1919,
    nzms13_1929,
    nzms13_1939,
    nzms15_1949,
    nzms_1959,
    nzms_1969,
    nzms_1979,
    nzms_1989,
    nzms260_1999,
    topo50,
    topo50_2019;

  nzms13_1899 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms13-1899/{z}/{x}/{-y}.png",
      numZoomLevels: 11,
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS13 1899",
    title: "NZMS13 1899",
    visible: false,
    projection: nztm,
    numZoomLevels: 11,
  });

  nzms13_1909 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms13-1909/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS13 1909",
    title: "NZMS13 1909",
    visible: false,
    projection: nztm,
  });

  nzms13_1919 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms13-1919/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS13 1919",
    title: "NZMS13 1919",
    visible: false,
    projection: nztm,
  });

  nzms13_1929 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms13-1929/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS13 1929",
    title: "NZMS13 1929",
    visible: false,
    projection: nztm,
  });

  nzms13_1939 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms13-1939/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS13 1939",
    title: "NZMS13 1939",
    visible: false,
    projection: nztm,
  });

  nzms15_1949 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms15-1949/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS15 1949",
    title: "NZMS15 1949",
    visible: true,
    projection: nztm,
  });

  nzms_1959 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms-1959/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS1 1959",
    title: "NZMS1 1959",
    visible: false,
    projection: nztm,
  });

  nzms_1969 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms-1969/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS1 1969",
    title: "NZMS1 1969",
    visible: false,
    projection: nztm,
  });

  nzms_1979 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms-1979/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS1 1979",
    title: "NZMS1 1979",
    visible: false,
    projection: nztm,
  });

  nzms_1989 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms-1989/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS1/260 1989",
    title: "NZMS1/260 1989",
    visible: false,
    projection: nztm,
    attributions: ["LINZ. CC BY 4.0"],
  });

  nzms260_1999 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/nzms260-1999/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZMS260 1999",
    title: "NZMS260 1999",
    visible: false,
    projection: nztm,
  });

  topo50 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/topo50/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZTM Topo 2009",
    title: "NZTM Topo 2009",
    visible: false,
    projection: nztm,
  });

  topo50_2019 = new TileLayer({
    source: new XYZ({
      crossOrigin: "anonymous",
      projection: nztm,
      url: "https://s3-ap-southeast-2.amazonaws.com/au.mapspast.org.nz/topo50-2019/{z}/{x}/{-y}.png",
      attributions: ["LINZ. CC BY 4.0"],
    }),
    type: "base",
    name: "NZTM Topo 2019",
    title: "NZTM Topo 2019",
    visible: false,
    projection: nztm,
  });

  const baseMaps = new LayerGroup({
    title: "Kōwhiritia tētehi mahere",
    layers: [
      nzms13_1899,
      nzms13_1909,
      nzms13_1919,
      nzms13_1929,
      nzms13_1939,
      nzms15_1949,
      nzms_1959,
      nzms_1969,
      nzms_1979,
      nzms_1989,
      nzms260_1999,
      topo50,
      topo50_2019,
    ],
  });

  return baseMaps;
}
