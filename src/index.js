import { register } from "ol/proj/proj4";
import proj4 from "proj4";
import { get as getProjection } from "ol/proj";
import Map from "ol/Map";
import View from "ol/View";
import LayerSwitcher from "embracethemirth-ol-layerswitcher";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import { defaults as defaultControls } from "ol/control.js";
import build_tile_layers from "./build_tile_layers.js";
import { TitleControl } from "./title_control.js";
import { ParcelsControl } from "./parcels_control.js";
import PopupFeature from "ol-ext/overlay/PopupFeature.js";
import Select from "ol/interaction/Select";
import { Style } from "ol/style";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import { savePDF } from "./save_pdf.js";
import get_key from "./get_key.js"
import { GraphControl } from "./graph_control.js";

document.title = NAME + " " + VERSION;
var LINZ_API_KEY;
// looks for a cached api key
// api key is only set if response.status == 200 in title_control
if (!localStorage.getItem("LINZ_API_KEY")){
  LINZ_API_KEY = get_key("tukua mai he LINZ API KEY");
} else {
  LINZ_API_KEY = localStorage.getItem("LINZ_API_KEY");
}

var extent = [-20037508, -20037508, 20037508, 20037508];


//var linzRef = "WN16B/193";
var highlightStyle = new Style({
  fill: new Fill({
    color: "rgba(255,255,255,0.6)",
  }),
  stroke: new Stroke({
    color: "#000000",
    width: 4,
  }),
});

proj4.defs(
  "EPSG:2193",
  "+proj=tmerc +lat_0=0 +lon_0=173 +k=0.9996 +x_0=1600000 +y_0=10000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
);
register(proj4); // register proj4 in openlayer after a proj4.defs
const nztm = getProjection("EPSG:2193");
nztm.setExtent(extent);

var view = new View({
  center: [1600000, 5500000],
  zoom: 1,
  projection: nztm,
  maxResolution: 4891.969809375,
  numZoomLevels: 11,
});

const vectorSource = new VectorSource();
const vectorLayer = new VectorLayer({
  source: vectorSource,
});

const myParcelControl = new ParcelsControl({
  map: map,
  vectorLayer: vectorLayer,
  vectorSource: vectorSource,
  nztm: nztm
});

const myGraphControl = new GraphControl({
  myParcelControl: myParcelControl
});

const pdf = new savePDF({});

var map = new Map({
  view: view,
  target: "map",
  layers: [build_tile_layers(nztm), vectorLayer],
  controls: defaultControls().extend([
    new TitleControl({
      LINZ_API_KEY: LINZ_API_KEY,
      myGraphControl: myGraphControl,
    }),
    myGraphControl,
    pdf,
  ]),
});

//map.addControl(pdf);

const layerSwitcher = new LayerSwitcher({
  reverse: false,
  groupSelectStyle: "group",
});

map.addControl(layerSwitcher);

//move view to be over recently added feature
//change zoom to invoke tilelayer
vectorSource.on("change", function (e) {
  var features = vectorSource.getFeatures();
  if (features.length > 0) {
    var lastElement = features[features.length - 1];
    view.fit(lastElement.getGeometry().getExtent());
    view.setZoom(10);
  }
});

var names = [
  "title_no",
  "par_id",
  "maori_name",
  "appellation_value",
  "parcel_type",
  "parcel_value",
  "second_prcl_value",
];

var attr = {};
names.forEach((n) => {
  attr[n] = {
    title: n,
    visible: function (v) {
      if (v.get(n) == null) {
        return false;
      } else return true;
    },
  };
});

var select = new Select({
  style: highlightStyle,
});

//double click to remove parcel from map
/*var unselect = new Select({
  condition: doubleClick,
});

unselect.on("select", function (e) {
  var selected = e.target.getFeatures();
  selected.forEach((v) => {
    vectorSource.removeFeature(v);
  });
});
map.addInteraction(unselect);
*/
map.addInteraction(select);

var popupFeature = new PopupFeature({
  popupClass: "default anim",
  select: select,
  canFix: true,
  template: {
    attributes: attr,
  },
});

map.addOverlay(popupFeature);
