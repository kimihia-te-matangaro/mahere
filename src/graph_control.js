import { SidePanel } from "embracethemirth-ol-side-panel";
import { WFS } from "ol/format";
import {
  and as andFilter,
  equalTo as equalToFilter,
  like as likeFilter,
  or as orFilter,
} from "ol/format/filter";
import * as d3 from "d3";

export class GraphControl extends SidePanel {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    let options = opt_options;

    const element = document.createElement("div");
    element.id = "d3";
    element.className = "d3";

    super({
      element: element,
      target: options.target,
    });

    this.myElement = element;
    this.myParcelControl = options.myParcelControl;
    this.data = { nodes: [], links: [], nodeids: new Set() };
    this.height = 700;
    this.width = 900;
  }

  //updates nodes and links based on response
  //one build only
  buildNetwork(response, titleRef, LINZ_API_KEY) {
  
    if (this.getPaneById("graph")) {
      this.empty();
    }
    const pane = this.definePane({
      paneId: "graph",
      icon: titleRef,
      name: " ",
    });
    pane.addWidgetElement(this.myElement);

    //if its a single node with no whakapapa
    if (response.single) {
      this.data.nodes.push({ id: response.title });
      this.data.nodeids.add(response.title);
      this.fetchTitleDetails(Array.from(this.data.nodeids), LINZ_API_KEY).then(
        (titleDetails) => {
          this.updateNodeData(titleDetails, titleRef);
        }
      );
    } else {
      response.features.forEach((v) => {
        if (v.properties.ttl_title_no_prior && v.properties.ttl_title_no_flw) {
          this.data.nodeids.add(v.properties.ttl_title_no_prior);
          this.data.nodeids.add(v.properties.ttl_title_no_flw);
          this.data.links.push({
            source: v.properties.ttl_title_no_prior,
            target: v.properties.ttl_title_no_flw,
          });
        }
      });
      //builds nodes from unique set of nodeids
      Array.from(this.data.nodeids).forEach((item) => {
        this.data.nodes.push({ id: item });
      });
      //adds title and date to nodes
      //draws graph
      this.fetchTitleDetails(Array.from(this.data.nodeids), LINZ_API_KEY).then(
        (titleDetails) => {
          this.updateNodeData(titleDetails, titleRef, LINZ_API_KEY);
        }
      );
    }
  }

  //check if nodes contain a given title
  //add title information to node
  updateNodeData(titleDetails, titleRef, LINZ_API_KEY) {
    let globalYMin;
    let globalYMax;
    let magicScale = 500;

    //pass title details to each node
    titleDetails.features.forEach((title) => {
      var nodeId = title.properties.title_no;
      var n = this.data.nodes.find((e) => e.id == nodeId);
      var date = new Date(title.properties.issue_date);
      n.id = nodeId;
      n.maori_land = title.properties.maori_land;
      n.issue_date = date;
    });

    //specifically highlights node that has been entered by user
    var chosenTitle = this.data.nodes.find((e) => e.id == titleRef);
    chosenTitle.chosen = true;

    //find latest and earliest dates
    this.data.nodes.forEach((n) => {
      if (n.issue_date && !globalYMin) {
        globalYMin = n.issue_date;
      } else if (n.issue_date && globalYMin) {
        if (n.issue_date < globalYMin) {
          globalYMin = n.issue_date;
        }
      }
      if (n.issue_date && !globalYMax) {
        globalYMax = n.issue_date;
      } else if (n.issue_date && globalYMax) {
        if (globalYMax < n.issue_date) {
          globalYMax = n.issue_date;
        }
      }
    });

    //assign a fixed y coordinate
    let scalerY = d3
      .scaleTime()
      .domain([globalYMin, globalYMax])
      .range([50, this.height - 50]);

    this.data.nodes.forEach((n) => {
      if (n.issue_date) {
        n.fy = scalerY(n.issue_date);
      }
    });

    //check if title has parcels
    //update node with parcels array
    this.myParcelControl
      .fetchParcelId(Array.from(this.data.nodeids), LINZ_API_KEY)
      .then((result) => {
        result.features.forEach((item) => {
          this.data.nodes.forEach((n) => {
            if (n.id == item.properties.ttl_title_no) {
              if (n.parcels) {
                n.parcels.push(item.properties.par_id);
              } else {
                n.parcels = [item.properties.par_id];
              }
            }
          });
        });
        //draw graph
        this.drawGraph(LINZ_API_KEY);
      });
  }

  drawGraph(LINZ_API_KEY) {
    const drag = function (simulation) {
      function dragsubject(event) {
        d3.select(this).attr("stroke", "black");
        return simulation.find(event.x, event.y);
      }

      function dragged(event, d) {
        d.fx = event.x;
        d.fy = event.y;
      }
      function dragended(event, d) {
        if (!event.active) simulation.alphaTarget(0);
        d.fx = event.x;
        d.fy = event.y;
      }

      function dragstarted(event) {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
        d3.select(this).attr("stroke", "white");
      }

      return d3
        .drag()
        .subject(dragsubject)
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
    };
    
    //force x and y are pulling nodes to the centre based on strength
    const simulation = d3
      .forceSimulation(this.data.nodes)
      .velocityDecay(0.1)
      .force("x", d3.forceX(this.width / 2).strength(.07))
      .force("y", d3.forceY(this.height / 2).strength(.07))
      .force("charge", d3.forceManyBody().strength(-240))
      .force(
        "link",
        d3.forceLink(this.data.links).distance(50).strength(0.1).id((d) => d.id)
      )
      // .force(
      //   "collision",
      //   d3.forceCollide().radius(function (d) {
      //     return 15;
      //   })
      // )
      .force("center", d3.forceCenter(this.width / 2, this.height / 2));

    const svg = d3
      .select(this.myElement)
      .append("svg")
      .attr("width", this.width)
      .attr("height", this.height)
      .classed("svg-content", true);

      let zoom = d3.zoom().scaleExtent([1, 10])
      .translateExtent([[0, 0], [this.width, this.height]]).on('zoom', handleZoom);
    
      function handleZoom(e) {
        svg.attr('transform', e.transform);
      }
      
      function initZoom() {
        svg.call(zoom);
      }
    
      initZoom();

    const node = svg
      .append("g")
      .attr("stroke", "#fff")
      .attr("stroke-width", 3)
      .selectAll("circle")
      .data(this.data.nodes)
      .join("circle")
      .attr("r", 10)
      .attr("fill", "rgba(182, 182, 182, 0.6)")
      .attr("class", "node")
      .call(drag(simulation));

  const arrow = svg.append("defs").append("marker")
      .attr("id", "arrow")
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 10)
      .attr("refY", 0)
      .attr("markerWidth", 5)
      .attr("markerHeight", 5)
      .attr("orient", "auto")
      .append("svg:path")
      .attr("d", "M0,-5L10,0L0,5");

    const path = svg.append("svg:g")
      .selectAll("line")
      .data(this.data.links)
      .enter().append("svg:path")
      .attr("class", "link")
      .attr("stroke-width", "10")
      .attr("stroke", "red")
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; })
      .attr("marker-end", "url(#arrow)");

  const link = svg.append("g")
      .attr("stroke", "rgba(182, 182, 182, 0.5)")
      .attr("stroke-width", 2)
      .selectAll("line")
      .data(this.data.links)
      .enter().append("line")
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; })
      .attr("marker-end", "url(#arrow)");
  
  // var path = svg.append("g").selectAll("path")
  //     .data(force.links())
  //   .enter().append("path")
  //     .attr("class", function(d) { return "link " + d.type; })
  //     .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });

    var texts = svg
      .selectAll("text.label")
      .data(this.data.nodes)
      .enter()
      .append("text")
      .attr("class", "label")
      .attr("fill", "rgba(28, 30, 32, 0.7)")
      .style("font-size", "11px")
      .text(function (d) {
        return d.id;
      });

    node.append("title").text((d) => d.issue_date + "\n" + "Māori Land: " + d.maori_land);


    simulation.on("tick", () => {
      link
        .attr("x1", (d) => Math.max(20, Math.min(this.width - 20, d.source.x)))
        .attr("y1", (d) => Math.max(20, Math.min(this.height - 20, d.source.y)))
        .attr("x2", (d) => Math.max(20, Math.min(this.width - 20, d.target.x)))
        .attr("y2", (d) => Math.max(20, Math.min(this.height - 20, d.target.y)));

      node
        .attr("cx", (d) => Math.max(20, Math.min(this.width - 20, d.x)))
        .attr("cy", (d) => Math.max(20, Math.min(this.height - 20, d.y)));

      texts.attr("transform", function (d) {
        return "translate(" + (d.x - 20) + "," + (d.y + 20) + ")";
      });
    });

    //selecting elligble nodes shows parcels
    var parcelControl = this.myParcelControl;
    var myData = this.data;
    d3.selectAll(".node").on("click", function (d) {
      if (d.target.__data__.parcels) {
        console.log(this);
        var clickedNode = myData.nodes.find(
          (e) => e.id == d.target.__data__.id
        );
        if (clickedNode.select) {
          clickedNode.select = false;
          d3.select(this).attr("fill", "rgba(0, 60, 136, 0.5)");
          parcelControl.removeParcel(clickedNode.parcels);
          return;
        } else if (!clickedNode.select) {
          clickedNode.select = true;
          d3.select(this).attr("stroke-width", 3).attr("stroke", "rgba(243, 211, 26, 0.7)")
          parcelControl.fetchParcelData(clickedNode.parcels, clickedNode.id, LINZ_API_KEY);
        }
      }
    });



    //highlight the nodes with parcels
    //highlight the chosen node
    d3.selectAll(".node").each(function (d) {
      if (d.parcels) {
        d3.select(this).attr("fill", "rgba(0, 60, 136, 0.5)");
      }
      if (d.chosen) {
        d3.select(this).attr("fill", "rgba(247, 100, 100, 0.5)").attr("r", 15);
      }
    });



    return svg.node();
  }

  empty() {
    d3.select("svg").remove();
    console.log(this.data);
    //clear features
    this.myParcelControl.vectorSource.clear();
    //clear data from network
    this.data = { nodes: [], links: [], nodeids: new Set() };
    //remove pane
    this.removePaneById("graph");
    return;
  }

  //gets given all of the node ids and finds title details for each
  //refs is an array
  fetchTitleDetails(refs,LINZ_API_KEY) {
    let myFilter;
    if (refs.length > 1) {
      myFilter = orFilter(...refs.map((e) => equalToFilter("title_no", e)));
    } else {
      //assume refs is not empty
      myFilter = equalToFilter("title_no", refs[0]);
    }
    const featureRequest = new WFS().writeGetFeature({
      featureTypes: ["table-52067"],
      request: "GetFeature",
      service: "WFS",
      outputFormat: "application/json",
      version: "2.0.0",
      filter: myFilter,
    });
    return fetch(
      "https://data.linz.govt.nz/services;key=" + LINZ_API_KEY + "/wfs?",
      {
        method: "POST",
        body: new XMLSerializer().serializeToString(featureRequest),
      }
    )
      .then((response) => response.json())
      .then((titleDetails) => {
        return titleDetails;
      });
  }
}
