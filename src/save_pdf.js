import { Control } from "ol/control.js";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";

export class savePDF extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    let options = opt_options;

    const button = document.getElementById("pdf-download");
    button.innerHTML = "&#8595;";
    const element = document.createElement("div");
    element.className = "pdf-download ol-control";
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });

    button.addEventListener("click", function () {
      html2canvas(document.body).then(function (canvas) {
        var image = canvas.toDataURL("image/png", 1.0);
        //width and length of the image
        var pdf = new jsPDF("p", "mm", [210, 222]);
        pdf.addImage(image, "JPEG", 0, 0);
        pdf.save("mahere.pdf");
        var map = pdf.output("Nō te taupānga nei");
        var downloadLink = document.getElementById("pdf-download");
        downloadLink.href = map;
      });
    });
  }
}
