import { Control } from "ol/control.js";
import { WFS } from "ol/format";
import {
  and as andFilter,
  equalTo as equalToFilter,
  like as likeFilter,
  or as orFilter,
} from "ol/format/filter";
import get_key from "./get_key.js"

export class TitleControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    let options = opt_options;
    const input = document.createElement("input");
    const button = document.createElement("button");
    button.innerHTML = "&#128269;";

    const element = document.createElement("div");
    element.className = "title-search ol-unselectable ol-control";
    element.appendChild(input);
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });
    this.input = input;
    this.LINZ_API_KEY = options.LINZ_API_KEY;
    this.myGraphControl = options.myGraphControl;

    function submitTitle() {
      var titleRef;
      if (this.element.children[0].value != "") {
        titleRef = this.element.children[0].value;
      }
      input.style.backgroundColor = "white";
      input.style.borderColor = "grey";
      input.style.opacity = "1";

      //checks if title ref is valid
      this.fetchTitleDetails(titleRef).then((validSearch) => {
        if (validSearch) {
          // since search was valid, save api key to cache
          window.localStorage.setItem("LINZ_API_KEY", this.LINZ_API_KEY)
          //sends response off to graph control
          let refSet = new Set([titleRef]);
          this.fetchTitleHierarchy(refSet).then((response) => {
            this.myGraphControl.buildNetwork(response, titleRef, this.LINZ_API_KEY);
            //go and get details for each node and pass them to the graph control
          });
        }
      })
    }
    button.addEventListener("click", submitTitle.bind(this), false);
  }

  fetchTitleDetails(linzRef) {
    if (linzRef != "") {
      const featureRequest = new WFS().writeGetFeature({
        featureTypes: ["table-52067"],
        request: "GetFeature",
        service: "WFS",
        outputFormat: "application/json",
        version: "2.0.0",
        filter: equalToFilter("title_no", linzRef),
      });
     return fetch(
        "https://data.linz.govt.nz/services;key=" + this.LINZ_API_KEY + "/wfs?",
        {
          method: "POST",
          body: new XMLSerializer().serializeToString(featureRequest),
        }
      )
        .then((response) => {
          if (response.status == 401){
            let message = "kua hē te LINZ API KEY, tukua mai anō"
            window.localStorage.removeItem('LINZ_API_KEY');
            this.LINZ_API_KEY = get_key(message);
            return null;
          }
          else {
            return response.json();
          }
        })
        .then((titleDetails) => {
          if (!titleDetails){
            return false;
          }
          else if (titleDetails.features.length > 0) {
            this.input.style.borderColor = "green";
            this.input.style.opacity = "0.6";
            return true;
          } else {
            alert("tukua mai he nama rerekē");
            this.input.style.borderColor = "red";
            this.input.style.backgroundColor = "grey";
            this.input.style.opacity = "0.8";
            return false;
          }
        });
    } else console.warn("kaore he taitara");
  }

  fetchTitleHierarchy(oldRefSet) {
    let newRefSet = new Set();
    let areSetsEqual = (a, b) =>
      a.size === b.size && [...a].every((value) => b.has(value));
    let linzRefList = Array.from(oldRefSet);
    // linzRefList is a set of titles
    // gets rows from the title hierarchy where the prior or following is in the set of titles linzRefList
    // returns a promise to the json response
    let myFilter;
    if (linzRefList.length > 1) {
      myFilter = orFilter(
        ...linzRefList.map((e) =>
          orFilter(
            equalToFilter("ttl_title_no_flw", e),
            equalToFilter("ttl_title_no_prior", e)
          )
        )
      );
    } else {
      myFilter = orFilter(
        equalToFilter("ttl_title_no_flw", linzRefList[0]),
        equalToFilter("ttl_title_no_prior", linzRefList[0])
      );
    }
    const featureRequest = new WFS().writeGetFeature({
      featureTypes: ["table-52011"],
      request: "GetFeature",
      service: "WFS",
      outputFormat: "application/json",
      version: "2.0.0",
      filter: myFilter,
    });
    return fetch(
      "https://data.linz.govt.nz/services;key=" + this.LINZ_API_KEY + "/wfs?",
      {
        method: "POST",
        body: new XMLSerializer().serializeToString(featureRequest),
      }
    )
      .then((response) => response.json())
      .then((result) => {
        if (result.features.length > 0) {
          result.features.forEach((v) => {
            if (v.properties.ttl_title_no_prior) {
              newRefSet.add(v.properties.ttl_title_no_prior);
            }
            if (v.properties.ttl_title_no_flw) {
              newRefSet.add(v.properties.ttl_title_no_flw);
            }
          });
          if (areSetsEqual(oldRefSet, newRefSet)) {
            return result;
          } else {
            return this.fetchTitleHierarchy(newRefSet);
          }
        } else {
          //if title has no whakapapa
          return { single: true, title: Array.from(oldRefSet)[0] };
        }
      });
  }
}
