const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
const webpack = require("webpack")

module.exports = {
  devServer: {
    contentBase: "./dist",
  },
  devtool: "inline-source-map",
  entry: "./src/index.js",
  mode: "development",
  module: {
    rules: [{ test: /\.css$/, use: 'raw-loader' }],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "src/mahere.css", to: "mahere.css" },
        { from: "node_modules/ol-ext/dist/ol-ext.css", to: "ol-ext.css" }
      ],
    }),
    new webpack.DefinePlugin({
      NAME: JSON.stringify(require("./package.json").name),
      VERSION: JSON.stringify(require("./package.json").version)
    })
  ],
};
